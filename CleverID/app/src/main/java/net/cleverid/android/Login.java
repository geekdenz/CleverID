package net.cleverid.android;

// see https://developers.google.com/games/services/training/signin
// and https://developers.google.com/+/mobile/android/sign-in

//import android.content.Context;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.api.client.util.DateTime;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

// the big one =
//import com.google.android.gms.common.GooglePlayServices doesn't exist!
//import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
//import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
//import com.google.android.gms.common.api.ResultCallback;


public class Login extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    private final String TAG = Login.class.getSimpleName();
    /**
     * True if the sign-in button was clicked.  When true, we know to resolve all
     * issues preventing sign-in without waiting.
     */
    //private boolean mSignInClicked;
    private boolean mSignInClicked = false;
    /**
     * True if we are in the process of resolving a ConnectionResult
     */
    private boolean mIntentInProgress;


    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;
    private static int GET_PROFILE_REQUEST = 1979;

    boolean mExplicitSignOut = false;
    boolean mInSignInFlow = false; // set to true when you're in the middle of the
    // sign in flow, to know you should not attempt
    // to connect in onStart()

    GoogleApiClient mGoogleApiClient;  // initialized in onCreate

    String mfirstName;
    String mlastName;
    String maddress1;
    String mdateOfBirth;
    String mcity;
    String mcountry;
    private String oauth2Token = null;
    // dialog for progress bar message
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
        */
        // Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());

        // Got to get a listener on the sign in button
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);

    }

    @Override
    protected void onStart() {
        Log.v(TAG, "Start... ");
        // every time start want to try to connect and return, no fuss
        // if successful we get a onConnected callback
        // failure = onConnectionFailed
        // OLD = mPlusClient.connect();

        super.onStart();
        if (!mInSignInFlow && !mExplicitSignOut) {
            // auto sign in
            mGoogleApiClient.connect();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }
        if (false && view.getId() == R.id.sign_in_button && !mGoogleApiClient.isConnecting()) {

            // ACTUALLY we should already be signed in on startup so
            Log.v(TAG, "tapped sign in!");
            // should have connection result ready to resolve
            mSignInClicked = true;


            //   // start the asynchronous sign in flow
            //   mSignInClicked = true;
            mGoogleApiClient.connect();

        } else if (view.getId() == R.id.sign_out_button) {
            // sign out.

            // user explicitly signed out, so turn off auto sign in
            mExplicitSignOut = true;
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                //Games.signOut(mGoogleApiClient);
                mGoogleApiClient.disconnect();
            }

            if (mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                mGoogleApiClient.disconnect();
                mGoogleApiClient.connect();
            }

            mSignInClicked = false;

            // show sign-in button, hide the sign-out button
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_button).setVisibility(View.GONE);
        }

    }


    @Override
    public void onConnected(Bundle connectionHint) {
        // show sign-out button, hide the sign-in button
        findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);

        // (your code here: update UI, enable functionality that depends on sign in, etc)
        mSignInClicked = false;
        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.v(TAG, "Connection Suspended");
    }

    public void onConnectionFailed(ConnectionResult result) {
        //
        Log.v(TAG, "Connection Failed");

        if (!mIntentInProgress) {
            if (mSignInClicked && result.hasResolution()) {
                // The user has already clicked 'sign-in' so we attempt to resolve all
                // errors until the user is signed in, or they cancel.
                try {
                    result.startResolutionForResult(this, RC_SIGN_IN);
                    mIntentInProgress = true;
                } catch (IntentSender.SendIntentException e) {
                    // The intent was canceled before it was sent.  Return to the default
                    // state and attempt to connect to get an updated ConnectionResult.
                    Log.v(TAG, "Another attempt?");
                    //mIntentInProgress = false;
                    //mGoogleApiClient.connect();
                }
            }
        }
    }

    private void updateUI(boolean signedIn) {
        View signInButton = findViewById(R.id.sign_in_button);
        View signOutButton = findViewById(R.id.sign_out_button);
        if (signedIn) {
            signInButton.setVisibility(View.INVISIBLE);
            signOutButton.setVisibility(View.VISIBLE);
        } else {
            signInButton.setVisibility(View.VISIBLE);
            signOutButton.setVisibility(View.INVISIBLE);
        }
    }
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            Toast toast = Toast.makeText(getApplicationContext(), acct.getDisplayName(), Toast.LENGTH_LONG);
            Log.v(TAG, acct.getDisplayName());
            EditText fullNameText = (EditText) this.findViewById(R.id.EDIT_TEXTLAST_NAME);
            fullNameText.setText(acct.getDisplayName());
            toast.show();
            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
            Toast toast = Toast.makeText(getApplicationContext(), "Sorry didnt get info", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    //protected void onActivityResult(int requestCode, int responseCode, Intent intent) {}
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        /*
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }

        // Check which request we're responding to
        if (requestCode == GET_PROFILE_REQUEST) {
            // Make sure the request was successful
            if (responseCode == RESULT_OK) {

                Bundle res = intent.getExtras();
                oauth2Token = res.getString("token");
                Log.d("\n", "token: " + oauth2Token + "\n");

                // get this new login
                new loginAsyncHttpTask().execute();
            }
        }
        */

    }


    //handle submit button
    public void submit(View button) {

        // collect the form data
        final EditText fullNameField = (EditText) findViewById(R.id.EDIT_TEXTLAST_NAME);
        mlastName = fullNameField.getText().toString();
        final EditText dateOfBirthField = (EditText) findViewById(R.id.EditTextdateOfBirthDay);
        final Spinner dateOfBirthMonthField = (Spinner) findViewById(R.id.SpinnerMonth);
        final EditText dateOfBirthYearField = (EditText) findViewById(R.id.EditTextdateOfBirthYear);
        int spinnerPosition = dateOfBirthMonthField.getSelectedItemPosition();
        String month = ""+ (spinnerPosition + 1);
        if (month.length() == 1) {
            month = "0"+ month;
        }
        mdateOfBirth = dateOfBirthYearField.getText().toString() +"-"+ month +"-"+ dateOfBirthField.getText().toString();
        final EditText address1Field = (EditText) findViewById(R.id.EditTextaddress1);
        maddress1 = address1Field.getText().toString();
        final EditText cityField = (EditText) findViewById(R.id.EditTextcity);
        mcity = cityField.getText().toString();
        final EditText countryField = (EditText) findViewById(R.id.EditTextcountry);
        mcountry = countryField.getText().toString();

        // get an oauth2 token...
        // make sure we have 1 google user
        // and they get a valid oauth2 token...
        /*
        Intent AccessTokenIntent = new Intent(getApplicationContext(), RetrieveAccessTokenActivity.class);
        startActivityForResult(AccessTokenIntent, GET_PROFILE_REQUEST);
        */
        (new SubmitDetailsTask()).execute(mlastName, mdateOfBirth, maddress1, mcity, mcountry);
    }
    private static class SubmitDetailsTask extends AsyncTask<String, Void, Void> {

        private static String TAG = SubmitDetailsTask.class.getCanonicalName();

        @Override
        protected Void doInBackground(String... params) {
            String name = params[0];
            String dateOfBirth = params[1];
            String address = params[2];
            String city = params[3];
            String country = params[4];
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dob = null;
            try {
                dob = formatter.parse(dateOfBirth);
            } catch (ParseException e) {
                Log.e(TAG, e.getMessage());
            }
            DateTime timeOfBirth = new DateTime(dob);
            try {
                GuidTaskControl.getMyApiService().
                        postUserData(address, city, country, timeOfBirth, name).execute();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
            return null;
        }
    }

    //
    // want 1 async task that gets called to GET id code and look at response
    public class loginAsyncHttpTask extends AsyncTask<String, Void, Integer> {

        String response = null;
        JSONObject mainObject = null;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(Login.this);
            dialog.setTitle("Working on registering new user...");
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(true);
            dialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpsURLConnection urlConnection = null;
            int statusCode = 0;
            String success_result;

            try {
            /* forming the java.net.URL object */
                // the problem with the login form is the parameters to POST are URL encoded eg. "New Zealand"
                // so build a uri...


                final String BASE_URL = EndPointServerURL.cleveridclient + "postUserData" + "?";

                final String ADDRESS1_PARAM = "address1";
                final String CITY_PARAM = "city";
                final String COUNTRY_PARAM = "country";
                final String DATE_OF_BIRTH_PARAM = "dateOfBirth";
                final String FIRST_NAME_PARAM = "firstName";
                final String LAST_NAME_PARAM = "lastName";

                Uri builtUri = Uri.parse(BASE_URL)
                        .buildUpon()
                        .appendQueryParameter(ADDRESS1_PARAM, maddress1)
                        .appendQueryParameter(CITY_PARAM, mcity)
                        .appendQueryParameter(COUNTRY_PARAM, mcountry)
                        .appendQueryParameter(DATE_OF_BIRTH_PARAM, mdateOfBirth)
                        .appendQueryParameter(FIRST_NAME_PARAM, mfirstName)
                        .appendQueryParameter(LAST_NAME_PARAM, mlastName)
                        .build();

                URL url = new URL(builtUri.toString());

                urlConnection = (HttpsURLConnection) url.openConnection();

            /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");
            /* optional request header */
                urlConnection.setRequestProperty("Authorization", "Bearer " + oauth2Token);
            /* for Get request */
                urlConnection.setRequestMethod("POST");
                statusCode = urlConnection.getResponseCode();
                Log.v(TAG, "Status code..." + statusCode + "\n");
                Log.v(TAG, "\n" + "the email scope token is... " + oauth2Token + "\n");

            /* 200 represents HTTP OK */
                if (statusCode == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    response = convertInputStreamToString(inputStream);

                    Log.v(TAG, "\n" + "Response: " + response + "\n");
                    try {

                        mainObject = new JSONObject(response);

                        success_result = mainObject.getString("success");
                        Log.v(TAG, " did it succeed? " + success_result);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    // for debugging I want to log the response here eg. 401
                    response = "0"; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }

            return statusCode; //"Failed to fetch data!";

        } // end of doInBackground

        @Override
        protected void onPostExecute(Integer result) {
        /* Download complete. Lets do something with it... */

            Context context = getApplicationContext();
            int duration = Toast.LENGTH_LONG;


            if (result == 200) {

                //Toast
                Toast toast = Toast.makeText(context, "It worked, you are now signed up with CleverId", duration);
                toast.show();

            } else {
                Log.e(TAG, "Failed!");

                //Toast
                Toast toast = Toast.makeText(context, result + " Oh no, sorry that failed, please try again", duration);
                toast.show();
            }


            dialog.dismiss();
            finish();

        }


        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;

            }

        /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

    } //end of class loginAsyncHttpTask


} // end of login
