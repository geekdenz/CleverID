package net.cleverid.android;


import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 */ // it has to be wrapped in asynctask class...
class UploadPhotoTask extends AsyncTask<String, Void, String> {

    private static final String TAG = UploadPhotoTask.class.getCanonicalName();

    public UploadPhotoTask() {
    }
    @Override
    protected String doInBackground(String... params) {
        String jsonUrl = "";
        try {
            URL url = new URL(Constants.GET_POST_IMAGE_URL);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            InputStream in = connection.getInputStream();
            //json = IOUtils.toString(in);
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            jsonUrl = readUrl(reader);
            Log.v(TAG, jsonUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonUrl;
    }

    public String readUrl(JsonReader reader) throws IOException {
        String url = null;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("url")) {
                url = reader.nextString();
                break;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return url;
    }

    @Override
    protected void onPostExecute(String token) {
        super.onPostExecute(token);

    }

} // end of class oauth2TokenTask
