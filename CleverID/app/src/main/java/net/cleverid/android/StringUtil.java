package net.cleverid.android;

import java.util.List;

/**
 * Created by denz on 12/1/15.
 */
public class StringUtil {
    public static String join(List<String> list, String separator) {
        String concat = "";
        int i = 0;
        for (Object item : list) {
            if (i++ > 0) {
                concat += separator;
            }
            concat += item.toString();
        }
        return concat;
    }
}
