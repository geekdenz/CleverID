package net.cleverid.android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.blunderer.materialdesignlibrary.activities.NavigationDrawerActivity;
import com.blunderer.materialdesignlibrary.handlers.ActionBarHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerAccountsHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerAccountsMenuHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerBottomHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerStyleHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerTopHandler;
import com.blunderer.materialdesignlibrary.models.Account;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import net.cleverid.android.events.OAuth2TokenEvent;
import net.cleverid.android.fragments.ChooseAccountFragment;
import net.cleverid.android.layout.MainMaterialFragment;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class MainMaterialActivity extends NavigationDrawerActivity implements GoogleApiClient.ConnectionCallbacks {
    private static final String TAG = MainMaterialActivity.class.getCanonicalName();
    private Map<String, ImageView> emailViewMap = new HashMap<>();
    private GoogleApiClient mGoogleApiClient;
    private NavigationDrawerTopHandler navigationDrawerTopHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        GuidModel model = new GuidModel(getApplicationContext());
        if (!model.hasGuids()) {
            showChooseAccount();
        }
    }

    /**
     * TODO: Implement this!
     */
    private void showChooseAccount() {
        //navigationDrawerTopHandler.getNavigationDrawerTopItems().set(0, findViewById(R.layout.fragment_choose_account));
        //navigationDrawerTopHandler.addItem(R.string.)
    }

    @Override
    public NavigationDrawerStyleHandler getNavigationDrawerStyleHandler() {
        return new NavigationDrawerStyleHandler();
    }

    //@Override
    public NavigationDrawerAccountsHandler getNavigationDrawerAccountsHandler() {
        return null;
        /*
        AccountManager manager = AccountManager.get(this.getApplicationContext());
        android.accounts.Account[] accounts = manager.getAccounts();
        String[] names = new String[accounts.length];
        NavigationDrawerAccountsHandler handler = new NavigationDrawerAccountsHandler(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addConnectionCallbacks(this)
                .build();
        mGoogleApiClient.connect();
        GoogleAccountManager am = new GoogleAccountManager(manager);
        //GoogleSignInAccount googleSignInAccount =

        //Person.Image image = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient).getImage();
        for (android.accounts.Account account : am.getAccounts()) {

            String name = account.name;
            Parcel parcel = Parcel.obtain();
            account.writeToParcel(parcel, BIND_IMPORTANT);
            if (!account.type.equals("com.google")) {
                continue;
            }
            //GoogleSignInAccount googleAccount = (GoogleSignInAccount) account;

            ImageView profile = new ImageView(this);
            emailViewMap.put(name, profile);
            handler.addAccount(name, account.type, profile.getDrawable(), R.drawable.background);
        }
        return handler;
        */
    }

    @Override
    public NavigationDrawerAccountsMenuHandler getNavigationDrawerAccountsMenuHandler() {
        return new NavigationDrawerAccountsMenuHandler(this)
                .addAddAccount(new Intent(getApplicationContext(), MainActivity.class))
                .addManageAccounts(new Intent(getApplicationContext(), MainActivity.class));
    }

    @Override
    public void onNavigationDrawerAccountChange(Account account) {
        Toast.makeText(getApplicationContext(), "Account changed!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public NavigationDrawerTopHandler getNavigationDrawerTopHandler() {
        navigationDrawerTopHandler = new NavigationDrawerTopHandler(this)
                .addItem(R.string.menu_qr_title, MainMaterialFragment.newInstance("",""))
                .addItem(R.string.menu_choose_account_title, new ChooseAccountFragment())
                .addItem(R.string.pref_updateFreq, new Intent(this, MainActivity.class))
                .addSection(R.string.pref_userProfile_title)
                .addItem(R.string.pref_updateFreqTitle, new Intent(this, MainActivity.class))
                .addItem(R.string.pref_title_display_name, new Intent(this, MainActivity.class));
        return navigationDrawerTopHandler;
    }

    @Override
    public NavigationDrawerBottomHandler getNavigationDrawerBottomHandler() {
        return new NavigationDrawerBottomHandler(this)
                .addSettings(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent);
                    }

                })
                .addHelpAndFeedback(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                });
    }

    @Override
    public boolean overlayActionBar() {
        return true;
    }

    @Override
    public boolean replaceActionBarTitleByNavigationDrawerItemTitle() {
        return true;
    }

    @Override
    public int defaultNavigationDrawerItemSelectedPosition() {
        return 0;
    }

    @Override
    protected boolean enableActionBarShadow() {
        return true;
    }

    @Override
    protected ActionBarHandler getActionBarHandler() {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        //emailViewMap.get(email);
        //mGoogleApiClient
        Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
        Person.Image image = person.getImage();


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void updateAuthToken(String oauth2Token) {
        Log.v(TAG, oauth2Token);
        GuidTaskControl control = new GuidTaskControl(this.getApplicationContext());
        //EventBus.getDefault().register(this);
        control.setOauth2Token(oauth2Token);
        control.doUntilTokenInvalid();
    }
    public void onEvent(OAuth2TokenEvent event) {
        updateAuthToken(event.message);
    }
}
