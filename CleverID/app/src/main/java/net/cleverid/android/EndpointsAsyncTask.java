package net.cleverid.android;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

import net.cleverid.backend.cleverid.Cleverid;

import java.io.IOException;
import java.util.List;

class EndpointsAsyncTask extends AsyncTask<Pair<Context, String>, Void, List<String>> {
    private static Cleverid myApiService = null;
    public static GoogleCredential credential = null;
    private Context context;

    @Override
    protected List<String> doInBackground(Pair<Context, String>... params) {
        if(myApiService == null) {  // Only do this once
            Cleverid.Builder builder = new Cleverid.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), credential)
                    // options for running against local devappserver
                    // - 10.0.2.2 is localhost's IP address in Android emulator
                    // - turn off compression when running against local devappserver
                    .setRootUrl(EndPointServerURL.cleverid);
            // See https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
            // end options for devappserver

            myApiService = builder.build();
        }

        context = params[0].first;
        String name = params[0].second;

        try {
            return myApiService.getIds(20).execute().getIds();
        } catch (IOException e) {
            Log.e(this.getClass().getName(), e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<String> result) {
        ((GetGuidsActivity) context).setGuids(result);
    }
}