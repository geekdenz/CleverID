package net.cleverid.android;

/**
 * Created by denz on 11/25/15.
 */
public class Constants {
    public static final String GUID_FILENAME = "OvPeiv2fa"; // secret
    public static final String BACKGROUND_SERVICE_BATTERY_CONTROL = "BACKGROUND_SERVICE_BATTERY_CONTROL";
    //public static final String PRIVATE_PREFERENCES = "PRIVATE_PREFERENCES";
    public static final String PRIVATE_PREFERENCES = "net.cleverid.android_preferences";
    public static final String GET_POST_IMAGE_URL = "https://cleveridme.appspot.com/photo?get_upload_url=true";
}
