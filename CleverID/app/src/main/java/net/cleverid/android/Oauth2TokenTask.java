package net.cleverid.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

import net.cleverid.android.events.OAuth2TokenEvent;

import java.io.IOException;

import de.greenrobot.event.EventBus;

/**
 */ // it has to be wrapped in asynctask class...
class Oauth2TokenTask extends AsyncTask<String, Void, String> {

    private static final String TAG = Oauth2TokenTask.class.getCanonicalName();
    private static final String googleSCOPE =
            "oauth2:https://www.googleapis.com/auth/userinfo.email";
    private static final int REQ_SIGN_IN_REQUIRED = 55664;
    private String email = null;
    private String oauth2Token;
    private Context context;

    public Oauth2TokenTask(Context activity) {
        this.context = activity;
    }
    public void startOauthAndGuidGettingProcess(Context context) {
        String email = Preferences.get("account", null);
        if (email != null) {
            //(new Oauth2TokenTask(context)).execute(email);
        }
    }
    @Override
    protected String doInBackground(String... params) {

        email = params[0];
        try {
            oauth2Token = GoogleAuthUtil.getToken(context.getApplicationContext(), email, googleSCOPE);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (UserRecoverableAuthException e) {
            //context.startActivityForResult(e.getIntent(), REQ_SIGN_IN_REQUIRED);
            EventBus.getDefault().post(email);
        } catch (GoogleAuthException e) {
            Log.e(TAG, e.getMessage());
        }

        return oauth2Token;
    }

    @Override
    protected void onPostExecute(String token) {
        super.onPostExecute(token);

        Log.e(TAG, "\n" + "The oauth2Token is... " + oauth2Token + "\n");

        Intent intent = new Intent();
        //bundle = getIntent().getExtras();
        intent.putExtra("token", oauth2Token);
        //context.setResult(Activity.RESULT_OK, intent);
        //context.updateAuthToken(oauth2Token);
        EventBus.getDefault().post(new OAuth2TokenEvent(oauth2Token));

        //mainActivity.finish();
    }

} // end of class oauth2TokenTask
