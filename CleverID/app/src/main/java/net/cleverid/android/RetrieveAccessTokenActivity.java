package net.cleverid.android;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.AccountPicker;


/*
     methods to get  access tokens
 */
public class RetrieveAccessTokenActivity extends Activity {
    //extends ActionBarActivity {

    private static final String TAG = "RetrieveAccessToken";

    private static final int REQUEST_CODE_PICK_ACCOUNT = 1000;

    private String mEmail; // Received from newChooseAccountIntent(); passed to getToken()

    // this scope is for a call to google
    private static final String googleSCOPE =
            "oauth2:https://www.googleapis.com/auth/userinfo.email";

    // token to acquire to access endpoints
    private String oauth2Token = null;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_retrieve_access_token);
        pickUserAccount();
    }

    // Right, shouldn't have to invoke the account picker every time
    // see https://developer.android.com/google/auth/http-auth.html#AccountPicker

    /**
     * Attempts to retrieve the username.
     * If the account is not yet known, invoke the picker. Once the account is known,
     * start an instance of the AsyncTask to get the auth token and do work with it.
     */

    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    // result of newChooseAccountIntent is handled here...

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            // Receiving a result from the AccountPicker
            if (resultCode == RESULT_OK) {
                mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

                // With the account name acquired, go get the auth token
                //new Oauth2TokenTask(this).execute(); // BREAKING CHANGE!
                //log it - its returned

            } else if (resultCode == RESULT_CANCELED) {
                // The account picker dialog closed without selecting an account.
                // Notify users that they must pick an account to proceed.
                Toast.makeText(this, R.string.pick_account, Toast.LENGTH_LONG).show();
            }
        }


        //more code can go here to handle the result from some exceptions...
    }


    // and this where we get oauth2


} // end of class RetrieveAccessTokenActivity






