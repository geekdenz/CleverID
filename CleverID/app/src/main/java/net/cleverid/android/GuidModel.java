package net.cleverid.android;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import nz.dcoder.security.Security;

/**
 * Created by denz on 12/10/15.
 */
public class GuidModel {
    private SharedPreferences preferences;
    private Context context;
    private Security security = new Security();
    private List<String> guids;
    private static int index = 0;

    public GuidModel(Context context) {
        this.context = context;
        updatePreferences();
    }
    public void initPrefs() {
        preferences = context.getSharedPreferences(Constants.PRIVATE_PREFERENCES, Context.MODE_PRIVATE);
    }
    public void updatePreferences() {
        initPrefs();
        guids = new ArrayList<>();
        String guidsAr[] = preferences.getString("guids", "").split(",");
        for (String guid : guidsAr) {
            this.guids.add(security.decrypt(guid));
        }
    }
    public String getNextGuid() {
        int size = guids.size();
        if (index >= size) {
            initPrefs();
            index = 0;
        }
        return guids.get(index++ % size);
    }
    public void write(List<String> guids) {
        initPrefs();
        SharedPreferences.Editor editor = preferences.edit();
        List<String> guidsEncrypted = new ArrayList<>(guids.size());
        for (String guid : guids) {
            guidsEncrypted.add(security.encrypt(guid));
        }
        String storedGuids = StringUtil.join(guidsEncrypted, ",");
        editor.putString("guids", storedGuids);
        editor.commit();
    }

    public boolean hasGuids() {
        return this.guids != null && guids.size() > 0;
    }
}
