package net.cleverid.android;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;
import java.util.List;

/*
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
*/


/**
 * This fragment reads GUIDs from the private file and DISPLAYS them
 *
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QRFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QRFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QRFragment extends Fragment implements View.OnClickListener {
    public final static int WHITE = 0xFFFFFFFF;
    public final static int BLACK = 0xFF000000;

    //  Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private final String TAG = QRFragment.class.getSimpleName();

    private static int counter = 0;
    private TextView mIDView = null;


    private String FILENAME = "guidsFileSecured";
    private List<String> guids = null;
    private String ids;
    private String[] idArray;
     /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1 = the GUID from Main (controller)
     * @param param2 Parameter 2.
     * @return A new instance of fragment QRFragment.
     */

    //  Rename and change types of parameters
    private String mGUID;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    // this context will survive fragment detaches Um = null pointer!!!
    //Context context = getActivity().getApplicationContext();

    public static QRFragment newInstance(String param1, String param2) {
        QRFragment fragment = new QRFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public QRFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGUID = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private LayoutInflater myInflater = null;
    private ViewGroup myContainer = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        myInflater = inflater;
        myContainer = container;

        // Inflate the layout for this fragment 1st
        View v = inflater.inflate(R.layout.fragment_qr, container, false);

        // here I can use container.find, getActivity().find
        // This is a bit of debug - counting times opened
        TextView mIDView = (TextView) v.findViewById(R.id.textcounter);

        mIDView.setText("GUID: " + mGUID + " ");
        mIDView.setTextSize(getResources().getDimension(R.dimen.textsize));
        mIDView.setTextColor(Color.parseColor("#0d5921"));
        Log.v(TAG, "GUID: "+mGUID);
        // ImageView to display the QR code in. This should be defined in
        // your Activity's XML layout file
        // OR
        // we could build it programatically with a minimum layout

        // This should be in an Async task too probably!

        ImageView imageView = (ImageView) v.findViewById(R.id.qrCode);
        int width = (int) getResources().getDimension(R.dimen.image_width);
        int height = (int) getResources().getDimension(R.dimen.image_height);

        Bitmap bm = null;
        try {
            bm = encodeAsBitmap(mGUID, width);
            imageView.setImageBitmap(bm);
        } catch (WriterException e) {
            Log.e(TAG, e.getMessage());
        }
        //imageView.setVisibility(View.INVISIBLE);
        /*
        ImageView barcodeImageView = (ImageView) v.findViewById(R.id.barCode);
        barcodeImageView.setVisibility(View.INVISIBLE);
        Bitmap barcodeBm = null;
        try {
            barcodeBm = ZXingTool.encodeAsBitmapBarcode(mGUID, 1000, 300);
        } catch (WriterException e) {
            Log.e(TAG, e.getMessage());
        }
        barcodeImageView.setImageBitmap(barcodeBm);
        */

        return v;

    }

    private Bitmap encodeAsBitmap(String str, int width) throws WriterException {
        BitMatrix result;
        try {
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hints = new Hashtable<>();
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, width, width, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, w, h);
        return bitmap;
    }

    //  Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    // This is the click for calling MainActivity
    @Override
    public void onClick(View v) {
        // calls the onFragmentInteraction method in MainActivity
        mListener.onFragmentInteraction(null);

    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public List<String> getGuids() {
        return guids;
    }

    public void setGuids(List<String> guids) {
        this.guids = guids;
    }

    public void setGuid(String guid) {
        this.mGUID = guid;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    /**
     * Writes the given Matrix on a new Bitmap object.
     * @param matrix the matrix to write.
     * @return the new {@link Bitmap}-object.
     */
    /*
    public static Bitmap toBitmap(BitMatrix matrix){
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
                bmp.setPixel(x, y, matrix.get(x,y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }
    */

}
