package nz.dcoder.security;

public class Security {

    private String key = null;
    private StringXORer stringXORer;

    public Security() {
        String keys[] = new String[] {
                "2d86f7bd10a042b6845abbad1673e19c",
                "b141d3be95b049bd8560b5450182c887",
                "37b11b35111140fb948dbbee96743786",
                "930947fe88cb4fd1818c0849c3c9f252",
                "eecd2ce32e53466394725d09cc7b36ab",
                "81af4a8f560343339cf1f970bc4875d0",
                "8bfd4a2a909c4dadb698c554d0c2409d",
                "699c5edf4e9c4c42973b58f387a1067d",
                "17949912b9304450aa695a5441d1c644",
                "12c38686c969487fb9b30ef0ff51cdf7",
                "2aa18ccd4d68475bbb9cd837a8ed47f5",
                "3b05dbd24302414d9c97c00762c70b76",
                "8b143e9fadd14c55984c1ad0f3717e3a",
                "70f234844a0146b3b7a124c211dbc8de",
                "4dd63787615d4c63bf76dafc1e55720a",
                "266bad4d730c4c1f8ff6bf48fe1e18c2",
                "24c87db74865445e8d45fa8eb223d982",
                "616015bff24c4278928e6103173e35f2",
                "7cd090a728fb4d1d909254ddd8aeadcc",
                "b7899482854e4480bae785928900c30a",
                "bee1d6f44030426290aece49d8c18148",
                "1b917a4081be4430ae679ac70a3e0a74",
                "6429d74255084688b8bb3e8bcc3bfe81"
        };
        String scrambled = keys[0];
        stringXORer = new StringXORer();
        for (int i = 1; i < keys.length; ++i) {
            scrambled = stringXORer.encode(scrambled, keys[i]);
        }
        key = scrambled;
    }

    public String encrypt(String plainText) {
        if (key.length() < plainText.length()) {
            throw new IllegalArgumentException("One Time Pad must be longer or equal length to Plain Text!");
        }
        return stringXORer.encode(plainText, key);
    }

    public String decrypt(String encrypted) {
        if (key.length() < encrypted.length()) {
            throw new IllegalArgumentException("One Time Pad must be longer or equal length to Plain Text!");
        }
        return stringXORer.decode(encrypted, key);
    }

    public static void main(String[] args) {
        System.out.println("Hello");
    }
}