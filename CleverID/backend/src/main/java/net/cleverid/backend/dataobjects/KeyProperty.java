/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cleverid.backend.dataobjects;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author denz
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface KeyProperty {
}
