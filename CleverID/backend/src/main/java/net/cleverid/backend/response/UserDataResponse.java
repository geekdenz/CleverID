/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cleverid.backend.response;

import com.google.appengine.api.users.User;
import java.util.Date;

/**
 *
 * @author denz
 */
public class UserDataResponse {
	private String email;
	private String fullName;
	private Date dateOfBirth;
	private String address1;
	private String address2;
	private String city;
	private String postcode;
	private String country;
	private String driversLicenseNumber;

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the user
	 */
	/*
	public User getUser() {
		return user;
	}
	*/

	/**
	 * @param user the user to set
	 */
	/*
	public void setUser(User user) {
		this.user = user;
	}
	*/

	/**
	 * @return the fullName
	 */
	public String getLastName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the driversLicenseNumber
	 */
	public String getDriversLicenseNumber() {
		return driversLicenseNumber;
	}

	/**
	 * @param driversLicenseNumber the driversLicenseNumber to set
	 */
	public void setDriversLicenseNumber(String driversLicenseNumber) {
		this.driversLicenseNumber = driversLicenseNumber;
	}

	/*
	public void setUserData(UserData userData) {
		this.address1 = userData.getAddress1();
		this.address2 = userData.getAddress2();
		this.city = userData.getCity();
		this.country = userData.getCountry();
		//this.dateOfBirth = userData.getDateOfBirth();
		this.driversLicenseNumber = userData.getDriversLicenseNumber();
		this.email = userData.getEmail();
		this.firstName = userData.getFirstName();
		this.fullName = userData.getLastName();
		this.middleName = userData.getMiddleName();
		this.postcode = userData.getPostcode();
		//this.user = userData.getUser();
	}
	*/
}
