/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.cleverid.response;

import com.google.gson.Gson;

/**
 *
 * @author tim
 */
public class UploadUrlResponse {
	private String url = null;
	private String blobKey = null;

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		String json = gson.toJson(this);
		return json;
	}

	@Override
	public String toString() {
		return this.toJson();
	}

	/**
	 * @return the blobKey
	 */
	public String getBlobKey() {
		return blobKey;
	}

	/**
	 * @param blobKey the blobKey to set
	 */
	public void setBlobKey(String blobKey) {
		this.blobKey = blobKey;
	}
}
