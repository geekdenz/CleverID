/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.cleverid.backend.dataobjects;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.users.User;
import java.util.Date;

/**
 *
 * @author denz
 */
@Persistent
public class UserData extends DataObject {

	@KeyProperty
	@Persistent
	private String email;

	@Persistent
	private User user;
	@Persistent
	private String fullName;
	@Persistent
	private Date dateOfBirth;
	@Persistent
	private String address1;
	@Persistent
	private String address2;
	@Persistent
	private String city;
	@Persistent
	private String postcode;
	@Persistent
	private String country;
	@Persistent
	private String driversLicenseNumber;

	public UserData(User user) {
		this.user = user;
		this.email = user.getEmail();
	}

	public UserData(String email) {
		this.email = email;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the full name to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public static UserData get(User user) {
		Filter filter = new FilterPredicate("user", FilterOperator.EQUAL, user);
		Iterable<Entity> entities = UserData.filterBy(UserData.class, filter);
		for (Entity entity : entities) {
			UserData userData = new UserData(user);
			userData.setFields(entity);
			return userData;
		}
		return null;
	}

	public static UserData get(String email) {
		Filter filter = new FilterPredicate("email", FilterOperator.EQUAL, email);
		Iterable<Entity> entities = UserData.filterBy(UserData.class, filter);
		for (Entity entity : entities) {
			UserData userData = new UserData(email);
			userData.setFields(entity);
			return userData;
		}
		return null;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the driversLicenseNumber
	 */
	public String getDriversLicenseNumber() {
		return driversLicenseNumber;
	}

	/**
	 * @param driversLicenseNumber the driversLicenseNumber to set
	 */
	public void setDriversLicenseNumber(String driversLicenseNumber) {
		this.driversLicenseNumber = driversLicenseNumber;
	}
}
