/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package net.cleverid.servlets;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

import net.cleverid.backend.response.UploadUrlResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {

	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws javax.servlet.ServletException if a servlet-specific error occurs
	 * @throws java.io.IOException if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/json;charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		String resource = request.getParameter("resource");
		//if (resource != null) {

			processResource(request, response, resource);
		//}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws javax.servlet.ServletException if a servlet-specific error occurs
	 * @throws java.io.IOException if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request servlet request
	 * @param response servlet response
	 * @throws javax.servlet.ServletException if a servlet-specific error occurs
	 * @throws java.io.IOException if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private void processResource(HttpServletRequest request, HttpServletResponse response, String resource) throws IOException {
		String myBlobKey = request.getParameter("blob-key");
        PrintWriter out = response.getWriter();
		BlobKey blobKey = null;
		if (myBlobKey != null) {
            response.setHeader("Content-Type", "image/jpeg");
			blobKey = new BlobKey(myBlobKey);
			blobstoreService.serve(blobKey, response);
			return;
		}
        if (request.getParameter("get_upload_url") != null) {
            response.getWriter().print("{\"url\": \""+
                    blobstoreService.createUploadUrl("/photo").replace("?get_upload_url=true/", "")
                            .replaceAll("neptune", "10.1.1.4") +
                    "\"}");
            return;
        }
        Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(request);
        List<BlobKey> blobKeys = blobs.get("myFile");

        if (blobKeys == null || blobKeys.isEmpty()) {
            //response.sendRedirect("/");
            out.println("empty");
        } else {
            //out.println(blobKeys.get(0).getKeyString() +"<br>");
            //out.println("/photo?blob-key=" + blobKeys.get(0).getKeyString());
            response.sendRedirect("/photo?blob-key=" + blobKeys.get(0).getKeyString());
        }
        /*
            return;

        if (resource != null) {
            switch (resource) {
                case "getUploadUrl":
                    out.print(getUploadUrl());
                    break;
                default:
                    UploadUrlResponse uur = new UploadUrlResponse();
                    uur.setUrl("INVALID REQUEST");
                    out.print(uur);
            }
        }
        */
	}

	private String getUploadUrl() {
		UploadUrlResponse uur = new UploadUrlResponse();
		//BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		String url = "/photo";
		String uploadUrl = blobstoreService.createUploadUrl(url);
		uur.setUrl(uploadUrl);
		uur.setBlobKey(uploadUrl.substring(uploadUrl.lastIndexOf('/') + 1));
		//blobstoreService.getBlobInfos()
		return uur.toJson();
	}
}
