function sort(paramString) {
	var params = paramString.split('&'),
		arr = [];
	$.each(params, function(i, param) {
		var parts = param.split('='),
			key = parts[0],
			value = parts[1];
		arr.push({
			key: key,
			value: value
		});
	});
	var sorted = arr.sort(function(a,b) {
		return a.key.localeCompare(b.key);
	});
	var ret = '',
		len = sorted.length;
	for (var i = -1; ++i < len;) {
		var el = sorted[i];
		if (i > 0) {
			ret += '&';
		}
		ret += el.key +'='+ el.value;
	}
	return ret;
}
(function($){
	//var hash = CryptoJS.SHA512("testtest");
	//$('.message').html('' + hash);
	$.get('GuidService', {n:'125d6d03b32c84d492747f79cf0bf6e179d287f341384eb5d6d3197525ad6be8e6df0116032935698f99a09e265073d1d6c32c274591bf1d0a20ad67cba921bc'}, function(salt) {
		$('form').
		submit(function(e) {
			var serialized = sort($('form').serialize()) +'&'+ 'salt='+ salt;
			console.log('serialized =', serialized);
			var myData = serialized +'&s='+ CryptoJS.SHA512(salt + serialized);
			console.log('myData = ', myData);
			$.ajax('SubmitDetails', {
				type: 'POST',
				data: myData,
				success: function(resp) {
					console.log(resp);
				}
			});
			return false;
		});
	}, 'text');
})(jQuery);